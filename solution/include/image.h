#ifndef IMAGE_H_
#define IMAGE_H_

#include <inttypes.h>

struct pixel {
    uint8_t b, g, r;
};

struct image {
  uint64_t width, height;
  struct pixel* data;
};

struct image img_create(uint64_t width, uint64_t height);

void img_destroy(struct image* img);

#endif
