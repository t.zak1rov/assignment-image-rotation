#ifndef ROTATE_
#define ROTATE_

#include "image.h"
#include <stddef.h>

void rotate(struct image* source);

#endif
