#include "file_handler.h"
#include "image.h"
#include "rotate.h"
#include <inttypes.h>
#include <stdio.h>


int main( int argc, char** argv ) {
    (void) argc; (void) argv;
    if (argc != 3) {
        fprintf(stderr, "2 arguments expected\n");
        return 1;
    }

    FILE* in = fopen(argv[1], "rb");
    if (in == NULL) {
        fprintf(stderr, "Error opening input file\n");
        return 1;
    }

    struct image img = {0};
    if (from_bmp(in, &img) != READ_OK){
        img_destroy(&img);
        fclose(in);
        return 1;
    }
    fclose(in);
    
    rotate(&img);

    FILE* out = fopen(argv[2], "wb");
    if (out == NULL) {
        img_destroy(&img);
        fprintf(stderr, "Error opening output file\n");
        return 1;
    }
    if (to_bmp(out, &img) != WRITE_OK) {
        img_destroy(&img);
        fclose(out);
        return 1;

    }
    fclose(out);
    img_destroy(&img);

    return 0;
}
