#include "image.h"
#include <stdio.h>
#include <stdlib.h>

struct image img_create(uint64_t width, uint64_t height){
    struct image img = {
        .width = width,
        .height = height,
        .data = malloc(sizeof(struct pixel) * width * height)
    };
    if (img.data == NULL) {
        fprintf(stderr, "Memory allocation failed: %s\n", __FUNCTION__);
        return (struct image) {0};
    }
    return img;
}

void img_destroy(struct image* img){
    if (img != NULL) {
        free(img->data);
    } 
    else {
        fprintf(stderr, "Cannot destroy nothing: %s\n", __FUNCTION__);
    }
}
