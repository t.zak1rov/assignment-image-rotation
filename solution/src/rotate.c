#include "rotate.h"
#include "image.h"
#include <stdio.h>
#include <stdlib.h>
#include <wchar.h>

void rotate (struct image* source) {
    struct image buf_img = img_create(source->width, source->height);
    for (size_t i = 0; i < source->width * source->height; ++i) {
        buf_img.data[i] = source->data[i]; 
    }
    for (size_t i = 0; i < source->height; ++i) {
        for (size_t j = 0; j < source->width; ++j) {
            source->data[(source->height - i - 1) + j * source->height] = buf_img.data[i*source->width + j];
        }
    }
    source->width = buf_img.height;
    source->height = buf_img.width;
    img_destroy(&buf_img);
}
